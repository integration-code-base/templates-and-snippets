function buildZeroQueries(products) {
  if (products.length === 0) return;

  return `<div class="digi-zero-products">${getProudcts(products)}</div>`;


  function getProudcts(products) {
    let productsTemplate = '';
    for (let i = 0; i < products.length; i++) {
      if (parseInt(products[i].price)) {
        productsTemplate += `<div class="digi-zero-product--item" data-id="${products[i].id}" position="${i}">
                  ${getDiscountLabel(products[i].price, products[i].oldPrice)}
                  <a  href="${products[i].link_url}" class="digi-zero-product-img-wrapper">
                    <img class="digi-zero-product-img" src="${products[i].image_url}" onerror="this.src='//cdn.diginetica.net/images/noimage.png'" alt="${products[i].name}" title="${products[i].name}">
                  </a>
      
                  <div class="digi-zero-product-name-wrapper">
                    <a href="${products[i].link_url}" class="digi-zero-product-name">${products[i].name}</a>
                  </div>
                  <div class="digi-price-wrapper">
                    ${getPrice(products[i].price, products[i].oldPrice)}
                  </div>
                  <div class="digi-zero-actions">
                    <a class="digi-zero-actions--detail" href="${products[i].link_url}">Подробнее</a>
                  </div>
                </div>`;
      }
    }
    return productsTemplate;

    function getDiscountLabel(price, oldPrice) {
      if (oldPrice) {
        return `<div class="digi-discount-label">
            <span class="digi-discount-label__title">СКИДКА</span>
            
            <span class="digi-discount-label__value"> ${Math.ceil(((oldPrice - price) / oldPrice) * 100)}%</span>
          </div>`
      }
      return '';
    }

    function getPrice(price, oldPrice) {
      return `${getOldPrice(oldPrice)}
                <div class="digi-zero-product-price__new">
                  <span class="digi-zero-product-price__value">
                    ${Number(price).toLocaleString('fr-FR')}
                  </span>
                  <span class="digi-zero-product-price__currency">руб.</span>
                </div>`;
    }

    function getOldPrice(oldPrice) {
      if (oldPrice) {
        return `<div class="digi-zero-product-price__old"> 
              <span class="digi-zero-product-price__value">
                ${Number(oldPrice).toLocaleString('fr-FR')}
                <sup>00</sup>
              </span>

              <span class="digi-zero-product-price__currency">руб.</span> 
            </div>`;
      }
      return '';
    }
  }
}

function getSearchTerm() {
  return decodeURIComponent(window.location.search.split('?q=')[1].split('&')[0]).replace(/\+(?=[^+])/g, ' ');
}

let autocompleteServiceUrl = `//autocomplete.diginetica.net/autocomplete?st=${getSearchTerm()}&apiKey=${Digi.config.anyQuery.autocomplete.apiKey}&productsSize=50&strategy=vectors_extended,zero_queries&regionId=${eval(Digi.config.tracking.commonParams.regionId.value)}`;

Digi.libraries.umbrella.ajax(autocompleteServiceUrl, {}, function (err, data) {

  function isZero() {
    return document.querySelector('#locayta-data-wrapper') === null; /* change selector for client */
  }

  Digi.libraries.common.waitForElement('.page-title').then(function () {
    if (isZero()) {
      Digi.libraries.umbrella.u('.digi-heading').after(buildZeroQueries(data.products));
    }
  });
});