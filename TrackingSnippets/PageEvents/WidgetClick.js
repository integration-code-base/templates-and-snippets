(function (expectedItemSelector, interval){
    window.digiCustom = window.digiCustom || {};
    let timerCounter = 0;
    let expectedItem = document.querySelector(expectedItemSelector);

    if(window.digiCustom.timerId) {
        clearTimeout(window.digiCustom.timerId);
    }

    window.digiCustom.timerId = setTimeout(function tick() {
        timerCounter++;
        expectedItem = document.querySelector(expectedItemSelector);
        if (expectedItem.style.display === 'block') {
            expectedItem.click();
        }
        if (timerCounter < 10 && expectedItem.style.display === 'none') {
            window.digiCustom.timerId = setTimeout(tick, interval); // (*)
        }
    }, interval)

})('#fastBuyResult', 1000)
