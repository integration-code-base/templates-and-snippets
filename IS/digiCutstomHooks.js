
/* Форс фокус инпута, баг на iphone, когда не происходит открытие автокомплита, но сам IS открылся */
if (this.state.isMobile) {
  setTimeout(function () {
    document.querySelector(window.Digi.config.anyQuery.instantSearch.inputSelector).focus();
  }, 100);
}