
// On App Create

/*
  Данный хук вешает лисенер на ресайз окна и вызывает методы продукта, которые правильно позиционируют наш виджет
  Кейс пока не покрыт на продукте по определенным причинам, поэтому используем данный хук
 */

window.addEventListener('resize', funcDigi.libraries.umbrella.ajaxtion () { App.methods.setCorrectPosition.bind({ $el: document.querySelector('#digi-shield') })(App.methods.getCoordinates.bind(App.data())())})


// On Container Open

/* Баг на айфоне при открытии ИС, данный хук форсит фокус в инпут для того, что бы открывались автоподсказки */
if (this.state.isMobile) { setTimeout(function () { document.querySelector('.digi-search-form__input-block input').focus()}, 100)};
