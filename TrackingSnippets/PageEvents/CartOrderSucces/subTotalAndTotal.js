/* simple */
(function () {
  var ecommerceArray = dataLayer.filter(function (x) {
    return x.ecommerce;
  });
  if (ecommerceArray.length) {
    return +ecommerceArray[0].ecommerce.purchase.actionField.revenue;
  }
  return null;
})();

/* try catch */

(function () {
  var collectedDataName = 'subtotal';
  window.tryCounter = window.tryCounter || 1;

  function whatAreWeWaiting() {
    return dataLayer.filter(function (x) {
      return x.ecommerce;
    }).length;
  }

  if (!whatAreWeWaiting()) {
    if (window.tryCounter < 7) {
      window.tryCounter++;
      throw new Error();
    } else {
      delete window.tryCounter;
      throw new Error('Could not collect ' + collectedDataName);
    }
  } else {
    function collectData() {
      return +dataLayer.filter(function (x) {
        return x.ecommerce;
      })[0].ecommerce.purchase.actionField.revenue.replace(/\,/g, '.');
    }

    return collectData();
  }
})();

/* Cart Items Iteration */

(function () {
  var result = 0;
  var filtredEcommerce = dataLayer.filter(function (x) {
    return x.ecommerce;
  });
  if (!filtredEcommerce.length) return;
  var items = filtredEcommerce[0].ecommerce.purchase.products;
  for (var i = 0; i < items.length; i++) {
    result += +items[i].quantity * +items[i].price;
  }
  return result;
})();
