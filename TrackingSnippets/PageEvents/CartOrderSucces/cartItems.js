/* Simple */
(function () {
  var result = [];
  var filtredEcommerce = dataLayer.filter(function (x) {
    return x.ecommerce
  });
  if (!filtredEcommerce.length) return;
  var items = filtredEcommerce[0].ecommerce.purchase.products;
  for (var i = 0; i < items.length; i++) {
    var item = {};
    item.productName = items[i].name;
    item.productId = items[i].id;
    item.price = +items[i].price;
    item.salePrice = item.price;
    item.quantity = +items[i].quantity;
    item.subtotal = item.salePrice * item.quantity;
    result.push(item);
  }
  return result;
})();

/* try catch Cart */

(function () {
  var collectedDataName = 'cartItems';
  var whatAreWeWaiting = dataLayer.filter(function (x) {
    return x.cart
  }).length;

  var errorMessage = 'Could not collect ' + collectedDataName;
  var digiWaitingFor = 'digiWaitingFor' + collectedDataName;
  window[digiWaitingFor] = window[digiWaitingFor] || 1;


  if (!whatAreWeWaiting) {
    if (window[digiWaitingFor] < 7) {
      window[digiWaitingFor]++;
      throw new Error();
    } else throw new Error(errorMessage);
  }

  function collectCartItems() {
    var result = [];
    var items = dataLayer.filter(function (x) {
      return x.cart
    })[0].cart.items;
    for (var i = 0; i < items.length; i++) {
      var item = {};
      item.productName = items[i].name;
      item.productId = items[i].sku;
      item.price = +items[i].price + '';
      item.salePrice = item.price + '';
      item.quantity = +items[i].quantity + '';
      item.subtotal = item.salePrice * item.quantity + '';
      result.push(item);
    }
    return result;
  }

  return collectCartItems();
})();

/* try catch Ecommerce */

(function () {
  var collectedDataName = 'cartItems';
  window.tryCounter = window.tryCounter || 1;

  function whatAreWeWaiting() {
    return dataLayer.filter(function (x) { return x.ecommerce }).length;
  }

  if (!whatAreWeWaiting()) {
    if (window.tryCounter < 7) {
      window.tryCounter++;
      throw new Error();
    } else {
      delete window.tryCounter;
      throw new Error('Could not collect ' + collectedDataName);
    }
  } else {
    function collectCartItems() {
      var result = [];
      var items = dataLayer.filter(function (x) {
        return x.ecommerce
      })[0].ecommerce.purchase.products;

      for (var i = 0; i < items.length; i++) {
        var item = {};
        item.productName = items[i].name;
        item.productId = items[i].id;
        item.price = +items[i].price;
        item.salePrice = item.price;
        item.quantity = +items[i].quantity;
        item.subtotal = item.salePrice * item.quantity;
        result.push(item);
      }
      return result;
    }

    return collectCartItems();
  }
})();

