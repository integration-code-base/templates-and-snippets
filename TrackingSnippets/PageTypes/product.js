(function () {
  var searchForField = 'pageType';
  var checkIfNeed = 'ProductPage';

  function searchObject() {
    var match = dataLayer.filter(function (x) {
      return x[searchForField];
    });
    if (match.length) {
      return match[0][searchForField];
    }
    return false;
  }

  function check() {
    return searchObject() === checkIfNeed;
  }

  return check();
})();

document.querySelector('#catalog-item') !== null;