function buildProducts(products) {
  if (products.length === 0) return;
  var html = '';
  html += '<div class="digi-zero-title">Возможно Вас заинтересует:</div>';

  html += '<div class="digi-zero-products">';

  for (var i = 0; i < products.length; i++) {
    var oldPrice = '';
    if (products[i].oldPrice) {
      oldPrice = '<div class="digi-zero-product-price__old">' +
        '<span class="digi-zero-product-price__value">' + Number(products[i].price).toLocaleString('fr-FR') + ' р.' + '</span>' +
        '</div>';
    }


    html += '<div class="digi-zero-product--item" id="' + products[i].id + '" position="' + i + '">' +
      '<a  href="' + products[i].link_url + '" class="digi-zero-product-img-wrapper">' +
      '<img class="digi-zero-product-img" src="' + products[i].image_url + '" alt="' + products[i].name + '" title="' + products[i].name + '">' +
      '</a>' +
      '<a href="' + products[i].link_url + '" class="digi-zero-product-title">' + products[i].name + '</a>';

    if (parseInt(products[i].price) !== 0) {
      html += '<div class="digi-zero-product-price__wrapper">' +
        oldPrice +
        '<div class="digi-zero-product-price__new">' +
        '<span class="digi-zero-product-price__value">' + Number(products[i].price).toLocaleString('fr-FR') + ' р.' + '</span>' +
        '</div>' +

        '</div>';
    }
    html += '</div>';
  }
  html += '</div>';
  return html;
}

function getRegionId() {
  var objectWithID = dataLayer.find(function (item) {
    return item.cityId;
  });
  return objectWithID.cityId;
}

function getSearchTerm() {
  return decodeURIComponent(window.location.search.split('?q=')[1].split('&')[0]).replace(/\+(?=[^+])/g, ' ');;
}

var autocompleteServiceUrl = '//autocomplete.diginetica.net/autocomplete?st=' + getSearchTerm() + ' ' + '&apiKey=' + Digi.config.anyQuery.autocomplete.apiKey + '&productsSize=50&strategy=vectors_strict,zero_queries' + '&regionId=global';

Digi.libraries.umbrella.ajax(autocompleteServiceUrl, {}, function (err, data) {

  function isZeroDekstop() {
    return document.querySelector('.catalogue__body') === null; /* change for client */
  }

  Digi.libraries.common.waitForElement('.search-result__steps.js-search-steps').then(function () {
    if (isZeroDekstop()) {
      console.log('Kus');
      Digi.libraries.umbrella.u('.digi-search-container').after(buildProducts(data.products));
    }
  });

  function isZeroMobile() {
    return false; /* change for client */
  }

  Digi.libraries.common.waitForElement('.products__wrap').then(function () {
    if (isZeroMobile()) {
      console.log('Kus mobile');
      Digi.libraries.umbrella.u('.products__wrap .not-found__by-query').after(buildProducts(data.products));
    }
  });
});