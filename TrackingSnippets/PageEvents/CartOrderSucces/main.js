/* set cartId */
(function() {
    var id = Math.round(
        100000 - 0.5 + Math.random() * (99999999999 - 100000 + 1)
    );
    var name = 'DIGI_CARTID';
    var digiCookie = name + '=' + id + '; path=/;';
    document.cookie = digiCookie
})();

/* take cartID */
if (document.cookie.split(';').filter(function(i){ return i.match('DIGI_CARTID')})[0]) {
    document.cookie.split(';').filter(function(i){ return i.match('DIGI_CARTID')})[0].split('=')[1]
}

/* try take cartId */

(function () {
    var collectedDataName = 'CartId';
    var whatAreWeWaiting = document.cookie.split(';').filter(function(i){ return i.match('DIGI_CARTID')})[0];

    var errorMessage = 'Could not collect ' + collectedDataName;
    var digiWaitingFor = 'digiWaitingFor' + collectedDataName;
    window[digiWaitingFor] = window[digiWaitingFor] || 1;


    if (!whatAreWeWaiting) {
        if (window[digiWaitingFor] < 7) {
            window[digiWaitingFor]++;
            throw new Error();
        } else throw new Error(errorMessage);
    }

    function collect() {
        return document.cookie.split(';').filter(function(i){ return i.match('DIGI_CARTID')})[0].split('=')[1]
    }

    return collect();
})();

/* Order ID */
if (dataLayer.filter(function(x){ return x.ecommerce }).length) {
    dataLayer.filter(function(x) { return x.ecommerce })[0].ecommerce.purchase.actionField.id;
}
