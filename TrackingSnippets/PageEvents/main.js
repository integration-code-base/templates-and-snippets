
/*isFromRedirect */
localStorage.digiSearchTerm !== undefined;


/* Search Term */
(function () {
  var searchTerm;
  if (localStorage.digiSearchTerm !== undefined) {
    searchTerm = localStorage.digiSearchTerm;
    localStorage.removeItem('digiSearchTerm'); // remove it so other events won't be affected
  }
  return searchTerm;
})();


/* try catch main */

(function () {
  var collectedDataName = 'searchPageProduct';
  window.tryCounter = window.tryCounter || 1;

  function whatAreWeWaiting() {
    if(dataLayer) {
      var objectsWithEcom = dataLayer.filter(function (x) {
        return x.ecommerce;
      })
      return objectsWithEcom && objectsWithEcom.length;
    }
    return  false;
  }

  if (!whatAreWeWaiting()) {
    if (window.tryCounter < 7) {
      window.tryCounter++;
      throw new Error();
    } else {
      delete window.tryCounter;
      throw new Error('Could not collect ' + collectedDataName);
    }
  } else {
    function collectData() {
    }

    return collectData();
  }
})();


(function () {
  var collectedDataName = 'categoryId';
  window.tryCounter = window.tryCounter || 1;

  function whatAreWeWaiting() {
    if(dataLayer) {
      var objectsWithEcom = dataLayer.filter(function (x) {
        return x.category_id;
      })
      return objectsWithEcom && objectsWithEcom.length;
    }
    return  false;
  }

  if (!whatAreWeWaiting()) {
    if (window.tryCounter < 7) {
      window.tryCounter++;
      throw new Error();
    } else {
      delete window.tryCounter;
      throw new Error('Could not collect ' + collectedDataName);
    }
  } else {
    function collectData() {
      return dataLayer.filter(function (x) {
        return x.category_id;
      })[0].category_id + ''
    }

    return collectData();
  }
})();

(function (){console.log(this);return document.location.search.match(/q=/) !== null;})()