/* classic dataLayer */
/* objectWithDetail not required but it's an additional
check for the correctness of the data selection  */
(function () {
  var result = '';
  var objectsWithDetail = dataLayer.filter(function (x) {
    return x.EventAction === 'detail';
  });
  if (objectsWithDetail.length) {
    var productsDetail = [];
    var objectsWithEcommerce = objectsWithDetail.filter(function (x) {
      return x.ecommerce;
    });

    objectsWithEcommerce.forEach(function (it) {
      if (it.ecommerce.detail !== undefined) {
        productsDetail = it.ecommerce.detail.products;
      }
    });
    result = productsDetail[0].id;
  }
  return result;
})();

/* classic dataLayer without objectWithDetail */
/* There are cases when the "detail" field contains a "view".
 I've seen this in unavailable products */

(function () {
  var result = '';
  var objectsWithEcommerce = dataLayer.filter(function (x) {
    return x.ecommerce;
  });
  if (objectsWithEcommerce.length) {
    var productsDetail = [];

    objectsWithEcommerce.forEach(function (it) {
      if (it.ecommerce.detail !== undefined) {
        productsDetail = it.ecommerce.detail.products;
      }
    });
    result = productsDetail[0].id;
  }
  return result;
})();


/* complicated id in dataLayer */
(function () {
  var searchForField = 'ecomm_prodid';

  function searchObjectInDataLayer() {
    var match = dataLayer.filter(function (x) {
      for (var y in x) {
        if (x[y] == 'page_view') return true;
      }
    });
    if (match.length) {
      return match[0][2][searchForField];
    } else {
      return false;
    }
  }

  function returnArray() {
    var objectInDataLayer = searchObjectInDataLayer();
    var productsArray = [];
    if (objectInDataLayer) {
      for (var pos of objectInDataLayer) {
        productsArray.push(pos + '');
      }

    } else {
      var productCards = document.querySelectorAll('.catalog-item');
      for (var i = 0; i < productCards.length; i++) {
        var splitedId = productCards[i].id.split('_');
        productsArray.push(splitedId[splitedId.length - 1]);
      }
    }
    return productsArray;
  }

  return returnArray();
})();

/* simple in dom */

(function (elem, value) {
  if (elem) {
    return elem[value];
  }
})(document.querySelector('input[name = "product_id"]'), 'value');

/* bitrix ids */

(function () {
  var result = [],
      items = document.querySelectorAll('.catalogElements .item');
  for (var i = 0; i < items.length; i++) {
    var splitedId = items[i].id.split('_');
    result.push(splitedId[splitedId.length - 1]);
  }
  return result;
})();


/* search in numbers named field productId*/
(function () {
  var searchForField = 'ecomm_prodid';

  function searchObject() {
    var match = dataLayer.filter(function (x) {
      for (var y in x) {
        if (x[y] == 'page_view') return true;
      }
    });

    if (match.length) {
      return match[0][2][searchForField];
    } else {
      var splitedId = document.querySelector('.bx-catalog-element').id.split('_');
      return splitedId[splitedId.length - 1];
    }
  }

  return searchObject();
})();

/* simple search productId*/

(function () {
  var searchForField = 'prodid';

  function searchObject() {
    var match = dataLayer.filter(function (x) {
      return x[searchForField];
    });

    if (match.length) {
      return match[0][searchForField];
    }
    return null;
  }

  return searchObject();

})();

/* try catch search productId not completed*/

(function () {
  var collectedDataName = 'productID';
  window.tryCounter = window.tryCounter || 1;

  function whatAreWeWaiting() {
    return dataLayer.filter(function (x) {
      return x.ecommerce;
    }).length;
  }

  if (!whatAreWeWaiting()) {
    if (window.tryCounter < 7) {
      window.tryCounter++;
      throw new Error();
    } else {
      delete window.tryCounter;
      throw new Error('Could not collect ' + collectedDataName);
    }
  } else {
    function collectData() {
      var objectsWithEcommerce = dataLayer.filter(function (x) {
        return x.ecommerce;
      });
      var productsDetail = [];
    
      objectsWithEcommerce.forEach(function (it) {
        if (it.ecommerce.detail !== undefined) {
          productsDetail = it.ecommerce.detail.products;
        }
      });
      return productsDetail[0].id;
    }
    return collectData();
  }
})();



