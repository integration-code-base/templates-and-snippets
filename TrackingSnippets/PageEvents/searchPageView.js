// searchTerm

/*
 This is a classic script for getting a query from url, but it has a problem with lookahead
 in replace, it looks for a plus character match in case if next character is not plus.
 BTW, plus it's just whitespace and there are a cases when user can enter several spaces
 then this script is not working well
 */
decodeURIComponent(window.location.search.split('q=')[1].split('&')[0]).replace(/\+(?=[^+])/g, ' ');

/*
  I don't know why, but we can just replace all the pluses in this case, I think the
  script was trying to avoid literally the plus character, but the plus it's another
  character in ASCII
  todo Test current and delete previous script
*/
decodeURIComponent(window.location.search.split('q=')[1].split('&')[0]).replace(/\+/g, ' ').trim();

/* anyQueryWorked default */
false;

/* anyQuerySynonyms default */
[];

/* originalSearchTerm default */
'';

/* Page products in dataLayer */
(function () {
  var productPage = [];

  if (dataLayer.filter(function (x) {
    return x.crto;
  }).length) {
    dataLayer.filter(function (x) {
      return x.crto;
    })[0].crto.products.forEach(function (it) {
      productPage.push(it);
    });
  }
  return productPage;
})();

/* tryCatch */
/*todo Написать и вынести отдельно снипет трай кетча с пояснением */
/* todo eccommerce вынести фильтр в переменную проверять на наличие и длину через "И". Пример: мобила сима ленд */

(function () {
  var collectedDataName = 'searchPageProduct';
  window.tryCounter = window.tryCounter || 1;

  function whatAreWeWaiting() {
    return dataLayer.filter(function (x) {
      return x.ecommerce;
    }).length;
  }

  if (!whatAreWeWaiting()) {
    if (window.tryCounter < 7) {
      window.tryCounter++;
      throw new Error();
    } else {
      delete window.tryCounter;
      throw new Error('Could not collect ' + collectedDataName);
    }
  } else {
    function collectData() {
      var result = [];
      var items = dataLayer.filter(function(x) { return x.ecommerce }).slice(-1)[0].ecommerce.impressions;
      for (var i = 0; i < items.length; i++) {
        var item = '';
        item= items[i].id+'';
        result.push(item);
      }
      return result;
    }

    return collectData();
  }
})();


/* Page products in DOM */
(function (selector, param) {
  var products = document.querySelectorAll(selector);
  var productsId = [];

  for (var i = 0; i < products.length; i++) {
    productsId.push(products[i].dataset[param]);
  }

  return productsId;
})('.catalog__items .item', 'product');


/* If we can't get product id we define zero queries or not with some check */
(function (productSelectorInSearchResults) {
  var id = [];
  if (document.querySelector(productSelectorInSearchResults) !== null) {
    id.push('not zero queries');
  }
  return id;
})('.products .products__item');

/*isFromRedirect */
localStorage.digiSearchTerm !== undefined;


/*originalSearchTerm post launch */
if (document.querySelector('.digi__result_initial-term a') !== null) {
  document.querySelector('.digi__result_initial-term a').innerText.toLowerCase().trim();
}


